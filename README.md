#	HAI913I TP5 [Spoon](https://spoon.gforge.inria.fr/) Logging

Ce projet, est la suite de ce [projet](https://gitlab.com/yanisallouch/hai913i-tp5-projet-de-test). S'il vous plait, veuillez lire le [README](https://gitlab.com/yanisallouch/hai913i-tp5-projet-de-test/-/blob/main/README.md) de ce projet là.

##	Introduction to software traceability with [Spoon](https://spoon.gforge.inria.fr/).

Trois processeurs ont été implémenté pour instrumenter le code :

1. [ClassProcessor.java](./traceability.with.spoon.tp5/src/main/java/processor/ClassProcessor.java)
	- Injecte les attributs de classes suivants : 
		1. `LOGGER` permet d'utiliser le framework de logging (avec une instanciation de ce LOGGER, voir ce [tutoriel](https://www.jmdoudoux.fr/java/dej/chap-logging.htm#logging-3)),
		2. `fh` permet de sauvegarder les logs dans un fichier géré par le handler par défaut,
		3. `e` concaténé a un timestamps, est utiliser pour l'ouverture du fichier par `fh`.

2. [ConstructorProcessor.java](./traceability.with.spoon.tp5/src/main/java/processor/ConstructorProcessor.java)
	- Injecte dans les constructeurs de classes, l'initialisation du `fh` déclaré en attribut de classe. 

3. [LoggingProcessor.java](./traceability.with.spoon.tp5/src/main/java/processor/LoggingProcessor.java)
	- Injecte une instruction de logging utilisant l'API [***Java Logging API***](https://docs.oracle.com/javase/8/docs/api/java/util/logging/package-summary.html), voir ce [tutoriel](https://www.jmdoudoux.fr/java/dej/chap-logging.htm#logging-3). Les injections de logging s'éffectue dans toutes les méthodes avec pour message, une concaténation de l'utilisateur courant et des paramètres de la méthode appelés si existants, sinon un message indiquant l'absence de parametres. **Attention**, Les injections ne s'effectuent **pas** dans les constructeurs de classes !
		1. Récupère le **WHO** de façon *hardcodé*.
		2. Est récupéré de façon **automatique** par le framework en utilisant l'instruction de logging `LOGGER.info(<methods parameters>)` :
			1. Le **WHAT**,
			2. Le **WHEN**, 
			3. Le **WHERE**.

###	Définition des 4W (tiré du cours)

>Les 4W est une notion abordé dans le module HAI913I, intitulé "Evolution et Restructuration Logiciel". La notion est développé par un intervenant extérieur a l'Université de Montpellier : [M. Bachar Rima](https://fr.linkedin.com/in/bachar-rima-4b618311b)

LPS context: the actual programming elements (variables, method calls, etc.) used to build the LPS event’s context which consists of four main parts:
- Who: Who triggered the event.
- When: When was the event triggered.
- Where: Where the event was triggered (e.g., GUI widget).
- What: What event information to retrieve (event type and other event-related data).

##	Options d'éxécution

Nous avons utilisé `org.apache.commons.cli` pour gerer les options en CLI.

Les options accéptées sont les suivantes :

- `-h` ou `help`
- `-si <path>` ou `sources-input` est **obligatoire**
	- Le chemin doit pointer a la racine du projet qui contiendra les fichiers sources Java (.java) a analysé.
- `-so <path>` ou `sources-output` est **obligatoire**
	- Le chemin doit pointer vers le dossier de sortie qui contiendra les fichiers sources Java (.java) voulu.
- `-bo <path>` ou `binaries-output` est facultatif,
	- Le chemin doit pointer vers le dossier de sortie qui contiendra les fichiers binaires Java (.class) voulu.
- `-cp <path>` ou `classpath` est facultatif,
	- Le classpath associé au projet Java source analysé.
- `-m` ou `maven`, facultatif.
	- Indique que le projet source analysé est un projet Maven. Cela permet de lancer le launcher Spoon approprié ([MavenLauncher](https://spoon.gforge.inria.fr/mvnsites/spoon-core/apidocs/spoon/MavenLauncher.html)). Attention, cela suppose que la variable M2_HOME, ou que la commande `mvn` existe dans le PATH.

##	How to run the application ?

Fournir a minima, l'option `si` vers le chemin dans le FS utilisé de l'application de test [ici](https://gitlab.com/yanisallouch/hai913i-tp5-projet-de-test), développé pour nos besoins et l'option `so` vers le dossier de sortie.


##	What is next ?

Une fois le projet de test spooned éxécuté et compris, vous pouvez consulter la suite dans ce [projet](https://gitlab.com/kaciahmed3/hai913i-tp5-question5) dévéloppé en continuité pour nos besoins permettant de parser un log et d'en extraire des profils utilisateurs (ayant servis comme objectif a la conception des LPS).

##	Remarques

###	Vis-à-vis du processeur n°1

L'injection de l'attribut n°1, permet d'importer par [Spoon](https://spoon.gforge.inria.fr/), l'instruction java suivante :
```java
import java.util.logging.Logger;
```

L'injection de l'attribut n°2, permet d'importer par [Spoon](https://spoon.gforge.inria.fr/), l'instruction java suivante :
```java
import java.util.logging.FileHandler;
```

L'injection de l'attribut n°3, permet d'importer par [Spoon](https://spoon.gforge.inria.fr/), l'instruction java suivante :
```java
import java.io.IOException;
```