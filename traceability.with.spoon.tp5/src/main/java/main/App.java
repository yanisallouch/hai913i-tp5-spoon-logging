package main;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import processor.ClassProcessor;
import processor.ConstructorProcessor;
import processor.LoggingProcessor;
import spoon.Launcher;
import spoon.MavenLauncher;
import spoon.MavenLauncher.SOURCE_TYPE;

/**
 * @author ALLOUCH Yanis
 * @author KACI Ahmed
 *
 */
public class App {

	static ClassProcessor classProcessor;
	static ConstructorProcessor constructorProcessor;
	static LoggingProcessor loggingProcessor;

	static String argSource = "";
	static String argOutput = "";
	static String argOutputBin = "";
	static String argClasspath = "";
	static String argMaven = "";

	public static void main(String[] args) throws ParseException {
		processArguments(args);
		Launcher spoon = configure();
		spoon.run();
	}

	private static void processArguments(String[] args) throws ParseException {
		final Options options = configParameters();
		final CommandLineParser parser = new DefaultParser();
		final CommandLine line = parser.parse(options, args);

		boolean helpMode = line.hasOption("help");// args.length == 0
		if (helpMode) {
			final HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("SpoonLogging", options, true);
			System.exit(0);
		}

		argSource = line.getOptionValue("sources-input", ".");
		argOutput = line.getOptionValue("sources-output");
		argOutput += File.separator + "spooned" + File.separator + "src";
		argOutputBin = line.getOptionValue("binaries-output", argOutput + File.separator + "bin");
		argClasspath = line.getOptionValue("classpath", "");
		argMaven = line.getOptionValue("maven");
	}

	private static Launcher configure() {
		Launcher spoon = new Launcher();
		if (argMaven != null) {
			spoon = new MavenLauncher(argSource, SOURCE_TYPE.APP_SOURCE);
		}
		spoon.addInputResource(argSource);
		spoon.setSourceOutputDirectory(argOutput);
		if (!argClasspath.equals("")) {
			spoon.getEnvironment().setSourceClasspath(new String[] { argClasspath });
		} else {
			spoon.getEnvironment().setNoClasspath(true);
		}
		spoon.setBinaryOutputDirectory(argOutputBin);
		spoon.getEnvironment().setAutoImports(true);
		// processeurs
		classProcessor = new ClassProcessor();
		constructorProcessor = new ConstructorProcessor();
		loggingProcessor = new LoggingProcessor();
		spoon.addProcessor(classProcessor);
		spoon.addProcessor(constructorProcessor);
		spoon.addProcessor(loggingProcessor);
		return spoon;
	}

	private static Options configParameters() {

		final Option helpFileOption = Option.builder("h").longOpt("help").desc("Affiche le menu d'aide.").build();

		final Option sourcesOption = Option.builder("si").longOpt("sources-input").hasArg(true)
				.argName("/chemin/vers/un/dossier/")
				.desc("Le chemin doit pointer a la racine du projet qui contiendra les fichiers sources Java (.java) a analysé.")
				.required(true).build();

		final Option outputOption = Option.builder("so").longOpt("sources-output").hasArg(true)
				.argName("/chemin/vers/un/dossier/")
				.desc("Le chemin doit pointer vers le dossier de sortie qui contiendra les fichiers sources Java (.java) voulu.")
				.required(true).build();

		final Option outputBinOption = Option.builder("bo").longOpt("binaries-output").hasArg(true)
				.argName("/chemin/vers/un/dossier/")
				.desc("Le chemin doit pointer vers le dossier de sortie qui contiendra les fichiers binaires Java (.class) voulu.")
				.required(false).build();

		final Option classpathOption = Option.builder("cp").longOpt("classpath").hasArg(true)
				.argName("classpath/du/projet/source").desc("Le classpath associé au projet Java source analysé.")
				.required(false).build();

		final Option mavenOption = Option.builder("m").longOpt("maven").hasArg(false).desc(
				"Indique que le projet source analysé est un projet Maven. Cela permet de lancer le launcher Spoon approprié (MavenLauncher). Attention, cela suppose que la variable M2_HOME, ou que la commande mvn existe dans le PATH.")
				.required(false).build();
		final Options options = new Options();
		options.addOption(sourcesOption);
		options.addOption(outputOption);
		options.addOption(outputBinOption);
		options.addOption(classpathOption);
		options.addOption(mavenOption);
		options.addOption(helpFileOption);

		return options;
	}
}
