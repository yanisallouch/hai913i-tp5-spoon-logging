package processor;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtCodeSnippetStatement;
import spoon.reflect.declaration.CtConstructor;

public class ConstructorProcessor extends AbstractProcessor<CtConstructor> {

	@Override
	public void process(CtConstructor element) {
		// Create statement for opening Handler declared as attribute of the class
		
		String statement = "try { fh = new FileHandler(\"" + element.getDeclaringType().getSimpleName()
				+ ".log\",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe(\"Impossible to open FileHandler\"); }"
				+ "catch (IOException e) { LOGGER.severe(\"Impossible to open FileHandler\"); }";
		final CtCodeSnippetStatement statementInConstructor = getFactory().Code().createCodeSnippetStatement(statement);
		// Add statement to the constructor body at begin (for better visualisation)
		element.getBody().addStatement(statementInConstructor);
	}
}
