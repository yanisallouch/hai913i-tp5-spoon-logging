package processor;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtCodeSnippetExpression;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.reference.CtTypeReference;

public class ClassProcessor extends AbstractProcessor<CtClass> {

	@Override
	public void process(CtClass element) {
		final CtTypeReference<Logger> loggerRef = getFactory().Code().createCtTypeReference(Logger.class);
		final CtTypeReference<FileHandler> fileHandlerRef = getFactory().Code().createCtTypeReference(FileHandler.class);
		final CtTypeReference<IOException> exceptionRef = getFactory().Code().createCtTypeReference(IOException.class);

		// Create static field LOGGER.
		final CtField<Logger> loggerField = getFactory().Core().<Logger>createField();
		loggerField.<CtField>setType(loggerRef);
		loggerField.<CtField>addModifier(ModifierKind.STATIC);
		loggerField.<CtField>addModifier(ModifierKind.PRIVATE);
		loggerField.setSimpleName("LOGGER");

		// Create field Handler.
		final CtField<FileHandler> handlerField = getFactory().Core().<FileHandler>createField();
		handlerField.<CtField>setType(fileHandlerRef);
		handlerField.<CtField>addModifier(ModifierKind.PRIVATE);
		handlerField.setSimpleName("fh");
		
		// Create field IOException for auto-imports.
		final CtField<IOException> exceptionField = getFactory().Core().<IOException>createField();
		exceptionField.<CtField>setType(exceptionRef);
		exceptionField.<CtField>addModifier(ModifierKind.PRIVATE);
		exceptionField.setSimpleName("e"+System.currentTimeMillis());


		// Create default statement value to initialize the LOGGER
		String expression = "Logger.getLogger(" + element.getSimpleName() + ".class.getName())";
		// no need for semi-colon; it is added by the SnippetExpression
		final CtCodeSnippetExpression loggerExpression = getFactory().Code().createCodeSnippetExpression(expression);
		loggerField.setDefaultExpression(loggerExpression);

		// Apply transformation. <=> Add the field to the concerned class in reverse
		// order
		element.addFieldAtTop(exceptionField);
		element.addFieldAtTop(handlerField);
		element.addFieldAtTop(loggerField);

	}

}
