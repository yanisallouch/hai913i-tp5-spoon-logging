package processor;

import java.util.List;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtCodeSnippetStatement;
import spoon.reflect.declaration.CtConstructor;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtParameter;
import spoon.support.reflect.declaration.CtConstructorImpl;

public class LoggingProcessor extends AbstractProcessor<CtExecutable> {

	@Override
	public boolean isToBeProcessed(CtExecutable candidate) {
		// Don't process Constructor methods
		if (candidate.getClass().getSimpleName().equals(CtConstructorImpl.class.getSimpleName())) {
			return false;
		}
		return true;
	}

	@Override
	public void process(CtExecutable element) {
		if (isToBeProcessed(element)) {
			CtCodeSnippetStatement snippet = getFactory().Core().createCodeSnippetStatement();

			// Snippet which contains the log.
			StringBuilder sb = new StringBuilder();
			List params = element.getParameters();
			if (isNotUserMethods(element)) {
				sb.append("App.getCurrentUser().toString()");
				sb.append("+ \";\" +");
			}
			if (params.size() == 0) {
				sb.append("\"no parameters given\"");
			} else {
				// penultimate the english translation of "avant-dernier"
				boolean isBeforePenultimate = true;
				for (int i = 0; i < params.size(); i++) {
					CtParameter param = (CtParameter) params.get(i);
					// make the hypothesis that the parameters has a toString() defined
					sb.append(param.getSimpleName() + ".toString()");
					isBeforePenultimate = i < (params.size() - 1);
					if (isBeforePenultimate) {
						sb.append("+ \";\" +");
					}
				}
			}

			final String value = String.format("LOGGER.info(%s)", sb.toString());
			snippet.setValue(value);

			// Inserts the snippet at the beginning of the method body.
			if (element.getBody() != null) {
				element.getBody().insertBegin(snippet);
			}
		}
	}

	private boolean isNotUserMethods(CtExecutable element) {
		return isNotGetCurrentUserMethod(element) && isNotToStringMethod(element);
	}

	private boolean isNotToStringMethod(CtExecutable element) {
		// Don't process toString() methods
		if (element.getSimpleName().equals("toString")) {
			return false;
		}
		return true;
	}

	private boolean isNotGetCurrentUserMethod(CtExecutable element) {
		// Don't process getCurrentUser() methods
		if (element.getSimpleName().equals("getCurrentUser")) {
			return false;
		}
		return true;
	}

}
